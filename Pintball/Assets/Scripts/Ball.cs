﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    private GameObject GameManager;
    private Rigidbody rbody;

    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        GameManager = GameObject.Find("GameManager");
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "incrScore1")
        {
            GameManager.GetComponent<GameManager>().incrScore(5);
        }
        else if (other.gameObject.tag == "incrScore2")
        {
            GameManager.GetComponent<GameManager>().incrScore(10);
        }
        else if (other.gameObject.tag == "incrScore3")
        {
            GameManager.GetComponent<GameManager>().incrScore(25);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "target1" || other.gameObject.tag == "target2" || other.gameObject.tag == "target3")
        {
            if (other.gameObject.tag == "target1")
            {
                GameManager.GetComponent<GameManager>().detectTarget(1);
            }
            else if (other.gameObject.tag == "target2")
            {
                GameManager.GetComponent<GameManager>().detectTarget(2);
            }
            else if (other.gameObject.tag == "target3")
            {
                GameManager.GetComponent<GameManager>().detectTarget(3);
            }
            GameManager.GetComponent<GameManager>().incrScore(5);
        }
        else if (other.gameObject.tag == "dead")
        {
            Destroy(this.gameObject);
            GameManager.GetComponent<GameManager>().restartBall();
        }
        else if (other.gameObject.tag == "exitMuelle")
        {
            GameManager.GetComponent<GameManager>().exitMuelle();
        }
    }
}
