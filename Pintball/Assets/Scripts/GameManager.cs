﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public int score, lifes = 0;
    [SerializeField]
    private Text textLife, textScore;
    [SerializeField]
    private Light lightTarget1, lightTarget2, lightTarget3; //luces
    [SerializeField]
    private GameObject ball, initTp, mainMuelle, target1, target2, target3, tpTarget1, tpTarget2, tpTarget3;
    private Transform initTarget1, initTarget2, initTarget3;
    private bool restartTime, restartTimeTargets, target1Down, target2Down, target3Down = false;
    private float time, timeTargets, timeRestartTargets= 0;

    void Start ()
    {
        initTarget1 = target1.transform;
        initTarget2 = target2.transform;
        initTarget3 = target3.transform;
        lightTarget1.GetComponent<Light>().color = Color.red;
        lightTarget2.GetComponent<Light>().color = Color.red;
        lightTarget3.GetComponent<Light>().color = Color.red;
    }
	
	void Update ()
    {
        textLife.GetComponent<Text>().text = "Lifes: " + lifes;
        textScore.GetComponent<Text>().text = "Score: " + score;
        controlTargets();
    }
    //TARGETS
    public void detectTarget(int numTarget)
    {
        switch (numTarget)
        {
            case 1: moveTarget(target1, tpTarget1); target1Down = true; lightTarget1.GetComponent<Light>().color = Color.green; break;
            case 2: moveTarget(target2, tpTarget2); target2Down = true; lightTarget2.GetComponent<Light>().color = Color.green; break;
            case 3: moveTarget(target3, tpTarget3); target3Down = true; lightTarget3.GetComponent<Light>().color = Color.green; break;
        }
        if (target1Down && target2Down && target3Down)
        {
            restartTimeTargets = true;
            target1Down = false; target2Down = false; target3Down = false;
        }
    }
    public void moveTarget(GameObject targetObj, GameObject tpTargetObj)
    {
        targetObj.transform.position = tpTargetObj.transform.position;
        targetObj.GetComponent<BoxCollider>().enabled = false;
    }
    public void removeTargets(bool all, int target = 0)
    {
        if (all)
        {
            target1.transform.localPosition = initTarget1.transform.position;
            target1.GetComponent<BoxCollider>().enabled = true;
            target2.transform.localPosition = initTarget2.transform.position;
            target2.GetComponent<BoxCollider>().enabled = true;
            target3.transform.localPosition = initTarget3.transform.position;
            target3.GetComponent<BoxCollider>().enabled = true;
            lightTarget1.GetComponent<Light>().color = Color.red;
            lightTarget2.GetComponent<Light>().color = Color.red;
            lightTarget3.GetComponent<Light>().color = Color.red;
        }
        else
        {
            switch (target)
            {
                case 1:
                    target1.transform.localPosition = initTarget1.transform.position;
                    target1.GetComponent<BoxCollider>().enabled = true;
                    lightTarget1.GetComponent<Light>().color = Color.red;
                    break;
                case 2:
                    target2.transform.localPosition = initTarget2.transform.position;
                    target2.GetComponent<BoxCollider>().enabled = true;
                    lightTarget2.GetComponent<Light>().color = Color.red;
                    break;
                case 3:
                    target3.transform.localPosition = initTarget3.transform.position;
                    target3.GetComponent<BoxCollider>().enabled = true;
                    lightTarget3.GetComponent<Light>().color = Color.red;
                    break;
            }
        }
    }
    private void controlTargets()
    {
        if (restartTimeTargets)
        {
            timeTargets = Time.deltaTime + timeTargets;
            if (timeTargets >= 25)
            {
                removeTargets(true);
                timeTargets = 0;
                restartTimeTargets = false;
            }
        }
        if (target1Down || target2Down || target3Down)
        {
            timeRestartTargets = Time.deltaTime + timeRestartTargets;
            if (timeRestartTargets >= 120)
            {
                if (target1Down)
                {
                    removeTargets(false, 1);
                    target1Down = false;
                }
                if (target2Down)
                {
                    removeTargets(false, 2);
                    target2Down = false;
                }
                if (target3Down)
                {
                    removeTargets(false, 3);
                    target3Down = false;
                }
                timeRestartTargets = 0;
            }
        }
        else
        {
            timeRestartTargets = 0;
        }
    }
    //TARGETS
    //SCORE
    public void incrScore(int _score)
    {
        if (restartTimeTargets)
        {
            _score = _score * 3;
        }
        score = score + _score;
    }
    //SCORE
    //MUELLE
    public void exitMuelle()
    {
        mainMuelle.GetComponent<Muelle>().activated = false;
    }
    //MUELLE

    public void restartBall()
    {
        if (lifes <= 0)
        {
            modeGameOver();
        }
        else
        {
            lifes = lifes - 1;
            mainMuelle.GetComponent<Muelle>().activated = true;
            Instantiate(ball, initTp.transform.position, this.transform.rotation);
        }
    }
    private void modeGameOver()
    {
        score = 0;
    }
}
