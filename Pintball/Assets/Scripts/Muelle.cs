﻿using UnityEngine;
using System.Collections;

public class Muelle : MonoBehaviour {
    public bool activated = false;
    [SerializeField]
    private float forceDown;
    private float forceUp = 0;
    private bool giveBackForce = false;
    private Rigidbody rbody;

    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        forceUp = forceDown + 5000000;
    }
    void Update()
    {
        if (activated)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                giveBackForce = false;
                rbody.AddForce(-transform.forward * forceDown);
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                rbody.AddForce(transform.forward * forceUp);
                giveBackForce = true;
            }
        }
        else
        {
            giveBackForce = true;
        }
        if (giveBackForce)
        {
            rbody.AddForce(transform.forward * forceUp);
        }
    }
}
