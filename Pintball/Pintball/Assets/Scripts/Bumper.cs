﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {
    [SerializeField]
    private float force = 1000f;
    [SerializeField]
    private float forceRadius = 1.0f;
    [SerializeField]
    private Light lightBumper;
    private bool lightActivated = false;
    private float tLight = 0;
    void OnCollisionEnter(Collision other)
    {
       foreach(Collider col in Physics.OverlapSphere(transform.position, forceRadius))
       {
          if (col.GetComponent<Rigidbody>())
          {
              col.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, forceRadius);
              lightActivated = true;
              tLight=0;
          }
       }
    }
    void Update()
    {
        if (lightActivated)
        {
            lightBumper.enabled = true;
            tLight = tLight + Time.deltaTime;
            if (tLight>=0.5f)
            {
                lightBumper.enabled = false;
                tLight = 0;
                lightActivated = false;
            }
        }
    }
}
