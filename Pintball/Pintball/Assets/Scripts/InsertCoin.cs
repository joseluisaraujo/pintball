﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertCoin : MonoBehaviour {
    [SerializeField]
    private GameObject GameManager, ball, initTp, Muelle;

    void OnMouseDown()
    {
        if (GameManager.GetComponent<GameManager>().lifes <= 0)
        {
            GameManager.GetComponent<GameManager>().lifes = 3;
            GameManager.GetComponent<GameManager>().score = 0;
            Instantiate(ball, initTp.transform.position, this.transform.rotation);
            Muelle.GetComponent<Muelle>().activated = true;
        }
    }
}
